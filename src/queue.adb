-- Copyright (c) 2013 Thomas Green & Will Speak
-- 
-- Distributed under the terms of the MIT Licence, see LICENCE.md for more
--information

package body Queue is
	protected body the_Queue is		
		entry Enq (Ball : in Element_T)
		when Space_Available_For_Enq is
		begin
			Q(Front) := Ball;
			Front := Front + 1;
			
			if Front = Back - 1 then
				Space_Available_For_Enq := False;
			end if;
			Items_Available_For_Deq := True;
		end Enq;
		
		entry Deq (Ball : out Element_T)
		when Items_Available_For_Deq is
		begin
			Ball := Q(Back);
			Back := Back + 1;
			
			if Front = Back then
				Items_Available_For_Deq := False;
			end if;
			Space_Available_For_Enq := True;
		end Deq;
		
		procedure Set_Last(Ball : in Element_T) is
		begin
			Set_Enth(0, ball);
		end Set_Last;
		
		procedure Set_Nth(item : in Q_Range; ball : in Element_T) is
		begin
			Set_Enth(Item, ball);
		end Set_Nth;
		procedure Set_enth(item : in Q_Range; ball : in Element_T) is
		begin
			Q(Front - (1 + item)) := Ball;
		end Set_enth;
		
	end the_Queue;
end Queue;

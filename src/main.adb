-- Copyright (c) 2013 Thomas Green & Will Speak
-- 
-- Distributed under the terms of the MIT Licence, see LICENCE.md for more
--information

with MaRTE_OS;
with Text_IO;
with Chute; use Chute;
with Ada.Calendar; use Ada.Calendar;
with Queue;

procedure Main is
	type Ball_Queue_Range is mod 5;
	package Ball_Queue is new Queue(Ball_Detected, Ball_Queue_Range);
	use Ball_Queue;
	Q : Ball_Queue.the_Queue;
	
	-- We are all done !
	Bail : Boolean := False;
	
	-- Time to wait between each ball
	rate : Duration := Duration(1);
	Timeout : Duration := Duration(5);
	Last_Release, Last_sorted : Time := Clock;
	
	-- Periodic task to release balls into the chute
	task type Releaser;
	task body Releaser is
		--Initalize time 
		Next : Time := Clock + Rate;
	begin
		Text_IO.Put_Line("Releaser_Starting");

		while not Bail loop
			
			if Last_release - Last_Sorted > Timeout then
				Bail := True;
			end if;
			
			-- Release a ball from the hopper
			Chute.Hopper_Load;
			delay Duration(0.15);
			last_release := Clock;
			Chute.Hopper_Unload;
			Q.Enq(Unknown);
			
			-- Periodic task stuff
			
			delay until next;
			next := next + rate;
		end loop;
		
		Text_Io.put_line("Hello");
	end Releaser;
	
	task type Sorter;
	task body Sorter is
		B : Ball_Detected := Unknown;
		T : Ada.Calendar.Time;
	begin
		Text_IO.Put_Line("Sorter_Starting");

		while not Bail loop
			
			-- We can block on this. This is a shame
			select
				delay Timeout;
			then abort
				Get_Ball(B,T);
			
				if B = Chute.Metal then
					rate := (T - last_release) + 0.17;
					Q.Set_Last(Metal);
				else
					Q.Deq(B);
					--delay Duration(0.02);
					Last_Sorted := T;
					if B = Metal then
						Sorter_Metal;
					else
						Sorter_Glass;
					end if;
				end if;
			end select;
		end loop;
		
		Text_Io.put_line("Cleaning Up...");
		
		Sorter_Metal;
		delay Duration(1);
		Sorter_Glass;
		delay Duration(1);
		Sorter_Close;
		
		Text_Io.put_line("World");
	end Sorter;
	
	R : Releaser;
	S : Sorter;
	
	B : Ball_Detected := Unknown;
	T : Ada.Calendar.Time;
begin
	-- discard phantom interrupts
	Chute.Get_Ball(B, T);
	--Chute.Get_Ball(B, T);
	
	-- onwards!!
end Main;

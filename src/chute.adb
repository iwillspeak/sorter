-- Copyright (c) 2013 Thomas Green & Will Speak
-- 
-- Distributed under the terms of the MIT Licence, see LICENCE.md for more
--information

with Ada.Calendar; use Ada.Calendar;
with Register_Control; use Register_Control;
with Ada.Interrupts;
with ada.interrupts.names;
with ada.text_io; use ada.text_io;

with queue;

package body Chute is

	type my_event is record
		t : time;
		b : ball_detected;
	end record;

	type interrupt_Queue_Range is mod 500;
	package interrupt_Queue is new Queue(my_event, interrupt_Queue_Range);
	use interrupt_Queue;

	protected type Handlers is
		procedure Handler_Serial;
		procedure Handler_Parallel;
		entry Get_Next(B: out Ball_Detected; T: out Time);
	private
		--Q : the_Queue;
		detected_time : Time;
		Ball : Ball_Detected;
		Valid : Boolean;
	end Handlers;

	H : Handlers;
	
	procedure Hopper_Load is
	begin
		Write_Hopper_Command_Bits(Load);
	end Hopper_Load;

	procedure Hopper_Unload is
	begin
		Write_Hopper_Command_Bits(Unload);
	end Hopper_Unload;

	procedure Sorter_Metal is
	begin
		Write_Sorter_Command_Bits(Open, Open);
	end Sorter_Metal;

	procedure Sorter_Glass is
	begin
		Write_Sorter_Command_Bits(Open, Closed);
	end Sorter_Glass;

	procedure Sorter_Close is
	begin
		Write_Sorter_Command_Bits(Closed, Closed);
	end Sorter_Close;

	procedure Get_Ball(B: out Ball_Detected; T: out Time) is
	begin
		H.Get_Next(B, T);
	end Get_Ball;

	protected body Handlers is
		procedure Handler_Serial is
			--e : my_event := (clock, Unknown);
		begin
			-- ignore spurious interrupts
			if not SP_Check_Interrupt then
				return;
			end if;

			Ball := Unknown;
			detected_time := Clock;
            --q.enq(e);		
			valid := True;
		end Handler_Serial;
	
		procedure Handler_Parallel is
			--e : my_event := (clock, Metal);
		begin
			Ball := Metal;
			detected_time := Clock;
            --q.enq(e);
			valid := True;
		end Handler_Parallel;

		entry Get_Next(B: out Ball_Detected; T: out Time)
		when Valid is
			--e : my_event;
		begin
			valid := false;
			--q.deq(e);
			--T := e.t;
			--B := e.b;
			t := detected_time;
			B := Ball;
			
		end Get_Next;
	end Handlers;


begin
	Put_Line("Register Handlers");
	
	Ada.Interrupts.Attach_Handler(H.Handler_Serial'access, Ada.Interrupts.Names.Serial_1);
	Ada.Interrupts.Attach_Handler(H.Handler_Parallel'access, Ada.Interrupts.Names.Parallel_1);	

	put_Line("Initialising_Interfaces");
	Initialize_Interfaces;
	
	put_Line("Waiting for Software Control");
	Wait_For_Software_Control;
end Chute;

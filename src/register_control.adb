-- Copyright (c) 2013 Thomas Green & Will Speak
-- 
-- Distributed under the terms of the MIT Licence, see LICENCE.md for more
--information

with Ada.Interrupts.Names; use Ada.Interrupts.Names;
with IO_Interface; use IO_Interface;
with Basic_Integer_Types; use Basic_Integer_Types;
with Ada.Unchecked_Conversion;

package body Register_Control is
	
	package Registers is 
		-- Serial port registers
		SP_BASE_REG    : constant IO_Port;
		SP_DATA_REG    : constant IO_Port;
		SP_DLLR_REG    : constant IO_Port;
		SP_IER_REG     : constant IO_Port;
		SP_DLHR_REG    : constant IO_Port;
		SP_IIR_FCR_REG : constant IO_Port;
		SP_LCR_REG     : constant IO_Port;
		SP_MCR_REG     : constant IO_Port;
		SP_LSR_REG     : constant IO_Port;
		SP_MSR_REG     : constant IO_Port;
		SP_SCRATCH_REG : constant IO_Port;
		
		-- Paralell Port registers
		PP_BASE_REG    : constant IO_Port;
		PP_DATA_REG    : constant IO_Port;
		PP_STATUS_REG  : constant IO_Port;
		PP_CONTROL_REG : constant IO_Port;
		
		-- PC Registers
		PC_IMR_REG : constant IO_Port;
	private
		-- Serial
		SP_BASE_REG    : constant IO_Port := 16#3F8#;
		SP_DATA_REG    : constant IO_Port := SP_BASE_REG + 0;
		SP_DLLR_REG    : constant IO_Port := SP_BASE_REG + 0;
		SP_IER_REG     : constant IO_Port := SP_BASE_REG + 1;
		SP_DLHR_REG    : constant IO_Port := SP_BASE_REG + 1;
		SP_IIR_FCR_REG : constant IO_Port := SP_BASE_REG + 2;
		SP_LCR_REG     : constant IO_Port := SP_BASE_REG + 3;
		SP_MCR_REG     : constant IO_Port := SP_BASE_REG + 4;
		SP_LSR_REG     : constant IO_Port := SP_BASE_REG + 5;
		SP_MSR_REG     : constant IO_Port := SP_BASE_REG + 6;
		SP_SCRATCH_REG : constant IO_Port := SP_BASE_REG + 7;
		
		-- paralell
		PP_BASE_REG    : constant IO_Port := 16#378#;
		PP_DATA_REG    : constant IO_Port := PP_BASE_REG + 0;
		PP_STATUS_REG  : constant IO_Port := PP_BASE_REG + 1;
		PP_CONTROL_REG : constant IO_Port := PP_BASE_REG + 2;
		
		-- PC
		PC_IMR_REG : constant IO_Port := 16#21#;
	end Registers;
	
	procedure Initialize_Interfaces is
		byte : Unsigned_8;
	begin
		-- initialise the parallel port
		-- Outb_P(Registers.PP_DATA_REG, 2#1100#);
		Outb_P(Registers.PP_CONTROL_REG, 2#00010000#);
		
		-- initialise the serial port
		-- TODO: initialise the serial port here
		--   * setup
		--   * enable interrupts
		
		-- unmask the interrupts
		byte := Inb_P(Registers.PC_IMR_REG);
		Outb_P(Registers.PC_IMR_REG, byte or 2#10010000#);
	end Initialize_Interfaces;
	
	procedure Write_Hopper_Command_Bits(Command : Hopper_Command) is
	begin
		-- TODO: Write command to the paralell port
		NULL;
	end Write_Hopper_Command_Bits;
	
	procedure Write_Sorter_Command_Bits(First : Sorter_Command;
	                                    Second :  Sorter_Command) is
	begin
		-- TODO: write the command bits to the parallel port
		NULL;
	end Write_Sorter_Command_Bits;
	
	function SP_Check_Interrupt return Boolean is
		reg : Unsigned_8;
	begin
		-- read the status register 
		reg := Inb_P(Registers.SP_MSR_REG);
		
		-- check the edge of the signal
		return (reg and 2#10001#) = 2#10001#;
	end SP_Check_Interrupt;

	procedure Wait_For_Software_Control is
		status : Unsigned_8 := Inb_p(Registers.PP_STATUS_REG);
	begin
		while (status and 2#01000000#) /= 0 loop
			status := Inb_p(Registers.PP_STATUS_REG);
		end loop;
	end Wait_For_Software_Control;

begin
	NULL;
end Register_Control;

-- Copyright (c) 2013 Thomas Green & Will Speak
-- 
-- Distributed under the terms of the MIT Licence, see LICENCE.md for more
--information

generic
	type Element_T is private;
	type Q_Range is mod <>;
package Queue is
	-- Types for queue
	type Q_Type is array(Q_Range) of Element_T;
	
	protected type the_Queue is
		entry Enq (Ball : in Element_T);
		entry Deq (Ball : out Element_T);
		procedure Set_Last(Ball : in Element_T);
		procedure Set_Nth(Item : in Q_Range; Ball : in Element_T);
	private
		procedure Set_Enth(Item : in Q_Range; Ball : in Element_T);
		Q: Q_Type;
		Space_Available_For_Enq: Boolean := True;
		Items_Available_For_deq: Boolean := False;
		Front, Back : Q_Range := Q_Range'First;
	end the_Queue;
end Queue;
